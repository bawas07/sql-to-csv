const {crud, csv} = require('./helpers')
const db = require('./models')
const { QueryTypes } = require('sequelize')

const sampleData = [
    {code: 'CA', name: 'California'},
    {code: 'TX', name: 'Texas'},
    {code: 'NY', name: 'New York'},
  ];

// const sequqlize = require('sequelize')

const run = async () => {
    try {
        const param = {
            activity_status_id: 1
        }
        const query = `
        SELECT 
        u.host as host,
        a.name as name,
        DATE_FORMAT(a.created_at, "%Y-%M-%d") as date_launched,
        a.total_download as total_download,
        a.activity_status_id as activity_status_id,
        IF(a.activity_status_id=1, 'On-Going', 'Finished') as status
        FROM activities a
        LEFT JOIN users u
        ON a.licensee_id = u.licensee_id
        WHERE NOT activity_status_id=2
        AND YEAR(a.created_at)=2020
        `
        const rawData = await db.sequelize.query(query, { type: QueryTypes.SELECT });
        // console.log({rawData})
        // const activity = await crud.findAllWithOpts('activities', {})
        // const data = JSON.parse(JSON.stringify(activity))
        const res = await csv.exportTo(rawData, './activity.csv')
        console.log({res})
    } catch (err) {
        return console.log({err});
    }
}

run()