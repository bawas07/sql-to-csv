const ObjToCsv = require('objects-to-csv');

module.exports = {
    exportTo: async (data, fileLocation) => {
        const csv = new ObjToCsv(data);
 
        // Save to file:
        await csv.toDisk(fileLocation);
       
        // Return the CSV file as string:
        const str = await csv.toString();
        return str;
    }
}