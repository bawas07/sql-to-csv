/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('default_value', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    feedback_correct_answer: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    image_correct_answer: {
      type: DataTypes.JSON,
      allowNull: true
    },
    feedback_partial_answer: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    image_partial_answer: {
      type: DataTypes.JSON,
      allowNull: true
    },
    feedback_wrong_answer: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    image_wrong_answer: {
      type: DataTypes.JSON,
      allowNull: true
    },
    licensee_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    deleted_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'default_value'
  });
};
