/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('player_activities', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    player_id: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    finish_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    score: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      defaultValue: 0.00
    },
    is_removed: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    distance: {
      type: DataTypes.DOUBLE,
      allowNull: true,
      defaultValue: 0
    },
    latitude: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0
    },
    longitude: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0
    },
    device_token: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    is_event_staff: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: 0
    },
    checkpoint_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: {
          tableName: 'checkpoints',
        },
        key: 'id'
      }
    },
    activity_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      references: {
        model: {
          tableName: 'activities',
        },
        key: 'id'
      }
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    deleted_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'player_activities'
  });
};
