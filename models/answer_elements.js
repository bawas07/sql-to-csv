/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('answer_elements', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    player_id: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    task_element_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      references: {
        model: {
          tableName: 'task_elements',
        },
        key: 'id'
      }
    },
    checkpoint_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      references: {
        model: {
          tableName: 'checkpoints',
        },
        key: 'id'
      }
    },
    answer: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    compressed: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: 1
    },
    is_removed: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: 0
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    times: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: 1
    },
    deleted_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'answer_elements'
  });
};
