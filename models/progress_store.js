/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('progress_store', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    progress_id: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    progress_value: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    progress_cache: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    progress_upload: {
      type: 'LONGTEXT',
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'progress_store'
  });
};
