/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('checkpoints', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    activity_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: {
          tableName: 'activities',
        },
        key: 'id'
      }
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    instruction: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    image: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    radius: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: 0
    },
    scale: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 2.40
    },
    is_hidden: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    is_permanent: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    is_instant: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    icon: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    latitude: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    longitude: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    callout_text: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    callout_image: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    is_use_customised_feedback: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    show_key: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 1
    },
    max_attempts: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: 1
    },
    allow_simultaneous_players: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 1
    },
    sound_mode: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 1
    },
    required_point: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: 0
    },
    allow_switch_hint_map: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 1
    },
    replace_guidance_with_hint: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    hint_image: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    hint_text: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    qr_code: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    max_concurrent_players: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    custom_path: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    is_start_check_point: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    is_finish_check_point: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    arrival_point: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: 0
    },
    route_type_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: {
          tableName: 'route_types',
        },
        key: 'id'
      }
    },
    checkpoint_type_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: {
          tableName: 'checkpoint_types',
        },
        key: 'id'
      }
    },
    checkpoint_status_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: {
          tableName: 'checkpoint_status',
        },
        key: 'id'
      }
    },
    task_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: {
          tableName: 'tasks',
        },
        key: 'id'
      }
    },
    answer_type_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: {
          tableName: 'answer_types',
        },
        key: 'id'
      }
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    deleted_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'checkpoints'
  });
};
