/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('keys', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    licensee_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    description: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    lifetime: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    deleted_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    is_lifetime: {
      type: DataTypes.STRING(5),
      allowNull: false,
      defaultValue: "off"
    },
    lifetime_days: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      defaultValue: 0
    },
    lifetime_hours: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      defaultValue: 0
    },
    lifetime_minutes: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      defaultValue: 0
    },
    is_ongoing: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      defaultValue: 0
    },
    parent_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: 0,
      comment: 'store parent id that cloned it'
    }
  }, {
    sequelize,
    tableName: 'keys'
  });
};
