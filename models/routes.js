/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('routes', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    time_limit: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    is_required: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    priority: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      defaultValue: 0
    },
    required_point: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: 0
    },
    award_point: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: 0
    },
    correctness: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: 0
    },
    checkpoint_source_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: {
          tableName: 'checkpoints',
        },
        key: 'id'
      }
    },
    checkpoint_destination_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: {
          tableName: 'checkpoints',
        },
        key: 'id'
      }
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    deleted_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'routes'
  });
};
