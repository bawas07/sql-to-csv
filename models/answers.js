/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('answers', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    answer: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
    is_controller_scored: {
      type: DataTypes.INTEGER(1),
      allowNull: false
    },
    score: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },
    player_id: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    time: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    time_arrival: {
      type: DataTypes.DATE,
      allowNull: true
    },
    time_spent_at_checkpoint: {
      type: DataTypes.DATE,
      allowNull: true
    },
    check_point_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      references: {
        model: {
          tableName: 'checkpoints',
        },
        key: 'id'
      }
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    times: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: 1
    },
    deleted_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'answers'
  });
};
