/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tasks', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    licensee_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    point: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: 100
    },
    point_decrease_step: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    feedback_correct_answer: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    image_correct_answer: {
      type: DataTypes.JSON,
      allowNull: true
    },
    feedback_partial_answer: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    image_partial_answer: {
      type: DataTypes.JSON,
      allowNull: true
    },
    feedback_wrong_answer: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    image_wrong_answer: {
      type: DataTypes.JSON,
      allowNull: true
    },
    max_attempt: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },
    time_limit: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    task_id: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    category_type: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: 3
    },
    folder_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: {
          tableName: 'folders',
        },
        key: 'id'
      }
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    deleted_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    is_ongoing: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: 0
    },
    parent_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: 0,
      comment: 'store parent id that cloned it'
    }
  }, {
    sequelize,
    tableName: 'tasks'
  });
};
