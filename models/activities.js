/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('activities', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    licensee_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    download_url: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    password: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    task_password: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    headline_color: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    sub_heading_color: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    primary_color: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    secondary_color: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    background_image: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    image: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    reduce_functionality: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    launch_user_auto: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    enable_map_view: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 1
    },
    allow_user_see_each_other: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 1
    },
    ignore_gps_positioning: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 1
    },
    disable_chat: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 1
    },
    sound_mode: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 1
    },
    personal_time_limit: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: 0
    },
    hide_real_time_ranking: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 1
    },
    hide_final_ranking_list: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    hide_real_time_scores: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 1
    },
    hide_all_answer_feedback: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 1
    },
    hide_travelled_distance: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    show_point: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    store_full_size: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    capabilities: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    custom_overlay_map: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 1
    },
    qr_code: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    total_download: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: 0
    },
    duration: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    color: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "red"
    },
    is_china: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    version: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      defaultValue: 0
    },
    link_to_join: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    link_to_join_2: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    chat_one_to_one: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    public_chat: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    public_chat_notification: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    zoom: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      defaultValue: 13
    },
    activity_status_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      references: {
        model: {
          tableName: 'activity_status',
        },
        key: 'id'
      }
    },
    template_name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    activity_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: {
          tableName: 'activities',
        },
        key: 'id'
      }
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    deleted_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    transparencies: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 1.00
    },
    is_block: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    overlay_image_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    isolated: {
      type: DataTypes.INTEGER(6),
      allowNull: true,
      defaultValue: 1
    },
    limit_download: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true
    },
    zipping: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: 0
    },
    finish_date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    upload_moved: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'activities',
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
};
