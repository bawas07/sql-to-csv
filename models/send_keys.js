/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('send_keys', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    player_id: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    activity_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: {
          tableName: 'activities',
        },
        key: 'id'
      }
    },
    confirm: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    inform: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 1
    },
    keys: {
      type: DataTypes.STRING(2048),
      allowNull: true
    },
    deleted_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'send_keys'
  });
};
