/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('map_overlay', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    transparencies: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: 100
    },
    min_zoom: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: 12
    },
    max_zoom: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: 18
    },
    activity_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: {
          tableName: 'activities',
        },
        key: 'id'
      }
    },
    image: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    sw_latitude: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    sw_longitude: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    ne_latitude: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    ne_longitude: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    zoom: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      defaultValue: 0
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    width: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0
    },
    height: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0
    },
    deleted_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    thumb: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    status: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "processing",
      comment: 'upload status: processing or uploaded'
    },
    processing_id: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: 'store current progress id'
    },
    is_ongoing: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'map_overlay'
  });
};
