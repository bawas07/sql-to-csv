/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('use_key', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    checkpoint_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: {
          tableName: 'checkpoints',
        },
        key: 'id'
      }
    },
    route_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: {
          tableName: 'routes',
        },
        key: 'id'
      }
    },
    type: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: 1,
      comment: '1 : required, 2: awarded'
    },
    key_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
      references: {
        model: {
          tableName: 'keys',
        },
        key: 'id'
      }
    },
    count: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      defaultValue: 1
    },
    ninja: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    deleted_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'use_key'
  });
};
